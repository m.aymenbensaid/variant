const readXlsxFile = require("read-excel-file/node");
const { writeFile } = require('fs/promises');
const short = require('short-uuid');
const { json2csv } = require("json-2-csv");

const mergedArray = (columns,res) => {
    return res.map((item) => {
        const obj = {};
        for (let i = 0; i < columns.length; i++) {
            obj[columns[i]] = item[i];
        }
        return obj;
    });
}

(async () => {
    const fileContent = await readXlsxFile('./sergent major.xlsx');
    const colunms = fileContent[0];
    const data = fileContent.slice(1);
    let result = [];

    data.forEach((d, i) => {
        const reference = d[1].split('-')[0];
        const variant = short.generate().slice(0, 6);
        const regex = new RegExp(reference, 'gi');

        if (!result.find(k => regex.test(k))) {
            d[0] = variant;
            result = [...result, d];
        }
        else {
            const founded = result.find(v => regex.test(v));
            d[0] = founded[0];
            result = [...result, d];
        }
    });

    const csv = await json2csv(mergedArray(colunms,result));
    writeFile(`output.csv`, csv);
    writeFile('output.json', JSON.stringify(mergedArray(colunms,result)));
    console.log(result.length)
    // console.log(result);
})();